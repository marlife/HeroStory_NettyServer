package org.tinygame.herostory.handler;

import com.google.protobuf.GeneratedMessageV3;
import io.netty.channel.ChannelHandlerContext;

/**
 * 指令处理器接口
 * @param <Cmd>
 */
public interface ICmdHandler<Cmd extends GeneratedMessageV3> {

    /**
     * 泛型 - 具体实现去声明参数类型
     * @param ctx
     * @param cmd
     */
    void handler(ChannelHandlerContext ctx,Cmd cmd);

}
