package org.tinygame.herostory.mode;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户管理器
 */
public final class UserManager {

    //私有构造
    private UserManager(){}

    /**
     * 用户字典，存储用户
     */
    private static final Map<Integer, User> userMap = new ConcurrentHashMap<>(100);

    /**
     * 添加用户
     * @param user
     */
    public static void addUser(User user){
        if(null == user){
            return;
        }
        userMap.put(user.getUserId(),user);
    }

    /**
     * 删除用户
     * @param userId
     */
    public static void removeUserById(Integer userId){
        if(null == userId){
            return;
        }
        userMap.remove(userId);
    }

    /**
     * 获取用户集合
     * @return
     */
    public static Collection<User> getUsers(){
        return userMap.values();
    }

    /**
     * 根据ID获取用户
     * @param userId
     * @return
     */
    public static User getUserById(int userId){
        return userMap.get(userId);
    }
}
