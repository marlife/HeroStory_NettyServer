package org.tinygame.herostory;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public final class BroadCaster {

    //私有构造
    private BroadCaster(){}

    /**
     * 客户端信道数组，一定要用static修饰，否则无法群发。因为这个handler，每次都是new出一个新的
     */
    private static final ChannelGroup group = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    /**
     * 添加信道
     * @param channel
     */
    public static void addChannel(Channel channel) {
        group.add(channel);
    }

    /**
     * 删除信道
     * @param channel
     */
    public static void removeChannel(Channel channel){
        group.remove(channel);
    }

    /**
     * 广播
     * @param msg
     */
    public static void broadCaster(Object msg){
        if(null == msg){
            return;
        }
        group.writeAndFlush(msg);
    }



}
